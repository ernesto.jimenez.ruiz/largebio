default: build dockerize

build:
	mvn clean package -U -Dmaven.test.skip=true

dockerize:
	docker build -f largebiobenchmarkcontroller.docker -t git.project-hobbit.eu:4567/ernestoj/largebio .
	docker build -f largebiodatagenerator.docker -t git.project-hobbit.eu:4567/ernestoj/largebiodatagenerator .
	docker build -f largebiotaskgenerator.docker -t git.project-hobbit.eu:4567/ernestoj/largebiotaskgenerator .
	docker build -f largebioevaluationmodule.docker -t git.project-hobbit.eu:4567/ernestoj/largebioevaluationmodule .

	docker push git.project-hobbit.eu:4567/ernestoj/largebio
	docker push git.project-hobbit.eu:4567/ernestoj/largebiodatagenerator
	docker push git.project-hobbit.eu:4567/ernestoj/largebiotaskgenerator
	docker push git.project-hobbit.eu:4567/ernestoj/largebioevaluationmodule