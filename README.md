# Largebio HOBBIT benchmark for the OAEI evaluation campaign

This **maven project** contains the necessary classes to implement the **largebio** benchmark to be run under the [HOBBIT platform](https://github.com/hobbit-project/platform/wiki/Upload-a-benchmark). 

The **largebio** track consists of finding alignments between the Foundational Model of Anatomy (FMA), SNOMED CT, and the National Cancer Institute Thesaurus (NCI). These ontologies are semantically rich and contain tens of thousands of classes. UMLS Metathesaurus has been selected as the basis for the track reference alignments. The ontologies and reference alignments are contained in the datasets folder.

### Benchmark classes

In HOBBIT the default workflow and format of task is rather generic and perhaps more oriented to query answering.
A System gets a source dataset (e.g. source ontology) and a Task that will contain the target dataset 
(e.g. target ontology) and expected results (e.g. reference alignment). For the OAEI LargeBio 
I have changed the workflow a bit and the source ontology is also provided within the task. This will enable to have
multiple tasks with different sources as in Conference and Multifarm tracks.

* **BenchmarkController**: this is the main class of the benchmark.
* **DataGenerator**: this class generates or points to the benchmark datasets (e.g. input ontologies and alignments) and prepares the datasets for the TaskGenerator. 
* For multiple-task benchmarks it is also important to define the queue names to be sent to the Sytem adapter.
* **Task**: includes the information of the source and target datasets and the expected results together with some other parameters like which type of entity should be matched. 
* **TaskGenerator**: deals with the generation of the task(s) and sends the task(s) to the System adapter and the EvaluationModule.
* **EvaluationModule**: compares the expected results (e.g. reference alignment) given by the TaskGenerator and the computed results by a system (e.g. computed alignment).

The above classes communicate to each other in a special way since they should be submitted to the platform	as docker images: [https://github.com/hobbit-project/platform/wiki/Push-a-docker-image](https://github.com/hobbit-project/platform/wiki/Push-a-docker-image)


### Benchmark metadata

The benchmark metadata is described in the **benchmark.ttl** file. 
This file should be added to a HOBBIT gitlab project (e.g. [https://git.project-hobbit.eu/ernestoj/largebio](https://git.project-hobbit.eu/ernestoj/largebio)).
To do so you should first create a user: [https://github.com/hobbit-project/platform/wiki/User-registration](https://github.com/hobbit-project/platform/wiki/User-registration)

A system participating in this Benchmark should explicitly mention that implements the API: **bench:LargebioAPI**. In 
practical terms the System adapter should interpret the information that is sent 
from the TaskGenerator and send the results to the EvaluationModule (a file containing the alignment in RDF Alignment format). 
See the implementation of the [LogMap system](https://gitlab.com/ernesto.jimenez.ruiz/logmap-hobbit) and  its [metadata description (system.ttl)](https://git.project-hobbit.eu/ernestoj/logmapsystem).


### Docker files and submission to the HOBBIT platform

Once the classes have been implemented, and a project has been created in the HOBBIT gitlab for each of the docker images. The next step is to create the docker images for each of the benchmark classes (4 in total) in the org.hobbit.largebiobenchmark package (e.g. **largebiobenchmarkcontroller.docker**) and push them to the HOBBIT platform. The docker files assume that a **maven package** for the benchmark has been created (e.g. target/LargebioBenchmark-1.0.0-SNAPSHOT.jar).

Each docker image should be push to a different project. The docker image for the BenchmarkController class has to be pushed to teh same gitlab project as the benchmark metadata (e.g. [https://git.project-hobbit.eu/ernestoj/largebio](https://git.project-hobbit.eu/ernestoj/largebio)). For the other classes we have to create devoted projects (e.g. largebiodatagenerator for the DataGenerator class).

The **Makefile** file contains the commands to create and push the docker images. For more information see [https://github.com/hobbit-project/platform/wiki/Push-a-docker-image](https://github.com/hobbit-project/platform/wiki/Push-a-docker-image)

To be allowed to push the docker images, you must use a personal access token with 'api' scope for **Git** over HTTP:
* Generate one at [https://git.project-hobbit.eu/profile/personal_access_tokens](https://git.project-hobbit.eu/profile/personal_access_tokens)
* Run ''docker login git.project-hobbit.eu:4567''. Use your HOBBIT gitlab user (e.g. ernestoj) and the generated token.
* Execute the Makefile (e.g. make).

In addition, Docker may require to be executed with root privileges.

If not you have not done it before you have to [install Docker CE](https://docs.docker.com/engine/installation/).


### HOBBIT gitlab projects

The created projects in the HOBBIT platform for largebio can be found under the [OAEI group](https://git.project-hobbit.eu/OAEI) or the [largebio-track group](https://git.project-hobbit.eu/OAEI/largebio-track).


### HOBBIT online instance

The online instance of the HOBBIT benchmarking platform is accessible at [master.project-hobbit.eu](https://master.project-hobbit.eu/home).


