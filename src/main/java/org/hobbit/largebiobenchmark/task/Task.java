/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package org.hobbit.largebiobenchmark.task;


import java.io.Serializable;


/**
 *
 * @author ernesto
 * Created on 9 Jan 2018
 *
 */
public class Task implements Serializable {
	
	private String taskId;
	private String taskQueueName; //To be use as queue name as well
	private byte[] source_target;
	private byte[] expectedAnswers;
	
	//To control what to match in the task
	private boolean isMatchingClassesRequired = true;
	private boolean isMatchingObjectPropertiesRequired = false;
	private boolean isMatchingDataPropertiesRequired = false;
	private boolean isMatchingInstancesRequired = false;
	
	
	
	public Task(String id) {
		this.taskId = id;

	}
	
	public void setId(String id) {
		this.taskId = id;
	}
	
	public String getTaskId() {
		return this.taskId;
	}
	    
	public void setSourceTarget(byte[] target) {
		this.source_target =  target;
	}
	
	public byte[] getSourceTarget() {
		return this.source_target;
	}
	    
	public void setExpectedAnswers(byte[] res) {
		this.expectedAnswers =  res;
	}
	
	public byte[] getExpectedAnswers() {
		return this.expectedAnswers;
	}

	/**
	 * @return the isMatchingClassesRequired
	 */
	public boolean isMatchingClassesRequired() {
		return isMatchingClassesRequired;
	}

	/**
	 * @param isMatchingClassesRequired the isMatchingClassesRequired to set
	 */
	public void setMatchingClassesRequired(boolean isMatchingClassesRequired) {
		this.isMatchingClassesRequired = isMatchingClassesRequired;
	}

	/**
	 * @return the isMatchingPropertiesRequired
	 */
	public boolean isMatchingDataPropertiesRequired() {
		return isMatchingDataPropertiesRequired;
	}

	/**
	 * @param isMatchingPropertiesRequired the isMatchingPropertiesRequired to set
	 */
	public void setMatchingDataPropertiesRequired(boolean isMatchingDataPropertiesRequired) {
		this.isMatchingDataPropertiesRequired = isMatchingDataPropertiesRequired;
	}

	/**
	 * @return the isMatchingInstancesRequired
	 */
	public boolean isMatchingInstancesRequired() {
		return isMatchingInstancesRequired;
	}

	/**
	 * @param isMatchingInstancesRequired the isMatchingInstancesRequired to set
	 */
	public void setMatchingInstancesRequired(boolean isMatchingInstancesRequired) {
		this.isMatchingInstancesRequired = isMatchingInstancesRequired;
	}

	/**
	 * @return the isMatchingObjectPropertiesRequired
	 */
	public boolean isMatchingObjectPropertiesRequired() {
		return isMatchingObjectPropertiesRequired;
	}

	/**
	 * @param isMatchingObjectPropertiesRequired the isMatchingObjectPropertiesRequired to set
	 */
	public void setMatchingObjectPropertiesRequired(boolean isMatchingObjectPropertiesRequired) {
		this.isMatchingObjectPropertiesRequired = isMatchingObjectPropertiesRequired;
	}

	/**
	 * @return the taskName or queueName to identify the files to send/receive
	 */
	public String getTaskQueueName() {
		return taskQueueName;
	}

	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskQueueName(String taskName) {
		this.taskQueueName = taskName;
	}
	

}
