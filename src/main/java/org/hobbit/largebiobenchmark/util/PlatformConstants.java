/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package org.hobbit.largebiobenchmark.util;

import java.io.File;

/**
 *
 * @author ernesto
 * Created on 9 Jan 2018
 *
 */
public class PlatformConstants {
	
	 // =============== DATA GENERATOR CONSTANTS ===============
    public static final String TASK = "largebio_task";
 
    // =============== EVALUATION MODULE CONSTANTS ===============

    public static final String EVALUATION_RECALL = "evaluation_recall";
    public static final String EVALUATION_PRECISION = "evaluation_precision";
    public static final String EVALUATION_FMEASURE = "evaluation_fmeasure";
    public static final String EVALUATION_TIME_PERFORMANCE = "evaluation_time_performance";

    
    
    // =============== INTERNAL FORLDERS FOR THE BENCHMARK =============== 
    public static String datasetsPath = System.getProperty("user.dir") + File.separator + "datasets";
    
    
    
    // =============== URIS (used in benchmark.ttl) ===============
    
    public static final String bench_ns = "http://w3id.org/bench#";
    
    //KPIs
    public static final String PRECISION_URI = bench_ns + "precision";
    public static final String RECALL_URI = bench_ns + "recall";
    public static final String FMEASURE_URI = bench_ns + "fmeasure";
    public static final String TIME_URI = bench_ns + "timePerformance";
    
    //Tasks
    public static final String TASK_FMA_NCI_SMALL_URI =  bench_ns + "FMA-NCI-Small";
    public static final String TASK_FMA_NCI_LARGE_URI =  bench_ns + "FMA-NCI-Large"; 
    public static final String TASK_FMA_SNOMED_SMALL_URI =  bench_ns + "FMA-SNOMED-Small";
    public static final String TASK_FMA_SNOMED_LARGE_URI =  bench_ns + "FMA-SNOMED-Large";
    public static final String TASK_SNOMED_NCI_SMALL_URI =  bench_ns + "SNOMED-NCI-Small"; 
    public static final String TASK_SNOMED_NCI_LARGE_URI =  bench_ns + "SNOMED-NCI-Large";
    
    
    
    public static final String TASK_FMA_NCI_SMALL =  "FMA-NCI-Small";
    public static final String TASK_FMA_NCI_LARGE =  "FMA-NCI-Large"; 
    public static final String TASK_FMA_SNOMED_SMALL = "FMA-SNOMED-Small";
    public static final String TASK_FMA_SNOMED_LARGE = "FMA-SNOMED-Large";
    public static final String TASK_SNOMED_NCI_SMALL = "SNOMED-NCI-Small"; 
    public static final String TASK_SNOMED_NCI_LARGE = "SNOMED-NCI-Large";
    
    //Parameters
    public static final String TASK_URI = bench_ns + "largebioTask";

    
    //Serialization Formats
    public static final String RDFXML = "RDFXML";
    public static final String TURTLE = "Turtle";
    
    //
    
    
}
