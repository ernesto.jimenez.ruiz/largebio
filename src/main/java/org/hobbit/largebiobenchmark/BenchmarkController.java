/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package org.hobbit.largebiobenchmark;

import org.apache.jena.rdf.model.NodeIterator;
import org.hobbit.core.Commands;
import org.hobbit.core.components.AbstractBenchmarkController;
import org.hobbit.largebiobenchmark.util.PlatformConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ernesto
 * Created on 9 Jan 2018
 *
 */
public class BenchmarkController  extends AbstractBenchmarkController {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BenchmarkController.class);
    private static final String DATA_GENERATOR_CONTAINER_IMAGE = "git.project-hobbit.eu:4567/ernestoj/largebiodatagenerator";
    private static final String TASK_GENERATOR_CONTAINER_IMAGE = "git.project-hobbit.eu:4567/ernestoj/largebiotaskgenerator";
    private static final String EVALUATION_MODULE_CONTAINER_IMAGE = "git.project-hobbit.eu:4567/ernestoj/largebioevaluationmodule";
	
    
    //KPIs: P, R, F and Time
    private String[] envVariablesEvaluationModule;
	

    @Override
    public void init() throws Exception {
        LOGGER.info("Initializing Benchmark Controller...");
        super.init();
        
        
        //Largebio has only one parameter. The name of the task. The name of the task is given to the 
        //data generator to get the required task resources (source, target and reference alignments)
        String largebio_task = (String) getProperty(PlatformConstants.TASK_URI, PlatformConstants.TASK_FMA_NCI_SMALL_URI);

        // data generators environmental values
        String[] envVariablesDataGenerator = new String[]{
            PlatformConstants.TASK + "=" + largebio_task
        };

        // get KPIs for evaluation module
        envVariablesEvaluationModule = new String[]{
            PlatformConstants.EVALUATION_RECALL + "=" + PlatformConstants.RECALL_URI,
            PlatformConstants.EVALUATION_PRECISION + "=" + PlatformConstants.PRECISION_URI,
            PlatformConstants.EVALUATION_FMEASURE + "=" + PlatformConstants.FMEASURE_URI,
            PlatformConstants.EVALUATION_TIME_PERFORMANCE + "=" + PlatformConstants.TIME_URI
        };

        //Create data generators.
        int numberOfDataGenerators = 1;
        createDataGenerators(DATA_GENERATOR_CONTAINER_IMAGE, numberOfDataGenerators, envVariablesDataGenerator);
        LOGGER.info("Initilalizing Benchmark Controller...");

               
        //Create task generators. 
        int numberOfTaskGenerators = 1;
        createTaskGenerators(TASK_GENERATOR_CONTAINER_IMAGE, numberOfTaskGenerators, new String[]{});
        LOGGER.info("Task Generators created successfully.");

        // Create evaluation storage
        createEvaluationStorage();
        LOGGER.info("Evaluation Storage created successfully.");

        waitForComponentsToInitialize();
        LOGGER.info("All components initilized.");
    }

    /**
     * A generic method for loading parameters from the benchmark parameter
     * model defined in the becnhmark.ttl file
     *
     * @param property the property that we want to load
     * @param defaultValue the default value that will be used in case of an
     * error while loading the property
     */
    @SuppressWarnings("unchecked")
    private <T> T getProperty(String property, T defaultValue) {
        T propertyValue = null;
        NodeIterator iterator = benchmarkParamModel
                .listObjectsOfProperty(benchmarkParamModel
                        .getProperty(property));
        if (iterator.hasNext()) {
            try {
//              
                if (defaultValue.equals(PlatformConstants.TASK_FMA_NCI_SMALL_URI)) { //In this case it is an URI!
                	
                	String value = iterator.next().asResource().getLocalName();
                	LOGGER.info("Returning value " + value + " for property " + property);                	
                    return (T) value;          
                } 
                //the case below are not used in largebio
                else if (defaultValue instanceof String) {
                    return (T) iterator.next().asLiteral().getString();
                } else if (defaultValue instanceof Integer) {
                    return (T) ((Integer) iterator.next().asLiteral().getInt());
                } else if (defaultValue instanceof Long) {
                    return (T) ((Long) iterator.next().asLiteral().getLong());
                } else if (defaultValue instanceof Double) {
                    return (T) ((Double) iterator.next().asLiteral().getDouble());
                }
            } catch (Exception e) {
            	propertyValue = defaultValue;
                LOGGER.error("Exception while parsing parameter.");
            }
        } else {
            LOGGER.info("Couldn't get property '" + property + "' from the parameter model. Using '" + defaultValue + "' as a default value.");
            propertyValue = defaultValue;
        }
        return propertyValue;
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hobbit.core.components.AbstractBenchmarkController#executeBenchmark()
     */
    @Override
    protected void executeBenchmark() throws Exception {

        // give the start signals
        sendToCmdQueue(Commands.TASK_GENERATOR_START_SIGNAL);
        sendToCmdQueue(Commands.DATA_GENERATOR_START_SIGNAL);
        LOGGER.info("Start signals sent to Data and Task Generators");

        // wait for the data generators to finish their work
        LOGGER.info("Waiting for the data generators to finish their work.");
        waitForDataGenToFinish();
        LOGGER.info("Data generators finished.");

        // wait for the task generators to finish their work
        LOGGER.info("Waiting for the task generators to finish their work.");
        waitForTaskGenToFinish();
        LOGGER.info("Task generators finished.");

        // wait for the system to terminate
        LOGGER.info("Waiting for the system to terminate.");
        waitForSystemToFinish();
        LOGGER.info("System terminated.");

        // create the evaluation module
        LOGGER.info("Will now create the evaluation module.");

        createEvaluationModule(EVALUATION_MODULE_CONTAINER_IMAGE, envVariablesEvaluationModule);
        LOGGER.info("Evaluation module was created.");

        // wait for the evaluation to finish
        LOGGER.info("Waiting for the evaluation to finish.");
        waitForEvalComponentsToFinish();
        LOGGER.info("Evaluation finished.");

        // Send the resultModule to the platform controller and terminate
        sendResultModel(this.resultModel);
        LOGGER.info("Evaluated results sent to the platform controller.");
    }

}
