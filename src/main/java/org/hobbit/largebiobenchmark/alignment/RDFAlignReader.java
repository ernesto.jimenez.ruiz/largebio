/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package org.hobbit.largebiobenchmark.alignment;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;



/**
 * This class transforms a RDF alignment (XML) file in a set of 
 * MappingObjectStr objects.
 * 
 * @author Ernesto
 *
 */
public class RDFAlignReader{

	public static final String CELL = "Cell";
	
	//Used in old gold standards
	private static final String ALIGNMENTENTITY1="alignmententity1";
	private static final String ALIGNMENTENTITY2 ="alignmententity2";
		
	private static final String ALIGNMENTRELATION="alignmentrelation";
	private static final String ALIGNMENTMEASURE="alignmentmeasure";
	
	
	public static final String ENTITY1 = "entity1";
	public static final String ENTITY2 = "entity2";
	public static final String RELATION = "relation";
	public static final String MEASURE = "measure";
	
	
 
	
	protected Set<MappingObjectStr> mappings = new HashSet<MappingObjectStr>();
	
	
	
	public Set<MappingObjectStr> getMappingObjects(){
		return mappings;
	}
	
	public HashAlignment getHashAlignment(){
		return new HashAlignment(mappings);
	}
	
	public int getMappingObjectsSize(){
		return mappings.size();
	}
	
	
	
	/**
	 * Added to read alignment as byte[] from the HOBBIT platform 
	 * @param data
	 * @throws Exception
	 */
	public RDFAlignReader(byte[] data) throws Exception {		
		this(new ByteArrayInputStream(data));
		
	}
	
	
	/**
	 * Reads from file containing alignment
	 * @param rdf_alignment_file
	 * @throws Exception
	 */
	public RDFAlignReader(String rdf_alignment_filepath) throws Exception {		
		this(new File(rdf_alignment_filepath));
		
	}
	
	
	/**
	 * Reads from file containing alignment
	 * @param rdf_alignment_file
	 * @throws Exception
	 */
	public RDFAlignReader(File rdf_alignment_file) throws Exception {		
		this(new FileInputStream(rdf_alignment_file));
		
	}
	
	
	
	public RDFAlignReader(InputStream is) throws Exception {
		
		//File xmlFile = new File(rdf_alignment_file);
		//InputStream is = new FileInputStream(xmlFile);
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader reader = factory.createXMLStreamReader(is);
		
		mappings.clear();
		
		String iri_str1="";
		String iri_str2="";
		String relation="";
		double confidence=0.0;
		int dir_relation;
		
		//int next=0;
		while(reader.hasNext())
		{	
			if(reader.getEventType()==XMLStreamConstants.START_ELEMENT){

				//System.out.println(next++);
	
				if (reader.hasName()){
			    	
			    	 if (reader.getLocalName().equals(RDFAlignReader.CELL)){
			    			iri_str1="";
			    			iri_str2="";
			    			relation="";
			    			confidence=0.0;
			    	 }
			    	 else if (reader.getLocalName().equals(RDFAlignReader.ENTITY1) ||
			    			 reader.getLocalName().equals(RDFAlignReader.ALIGNMENTENTITY1)){
			    		 
			    		 if (reader.getAttributeCount()>0){
				    		//System.out.println("Att: " + reader.getAttributeValue(0));
			    			 iri_str1 = reader.getAttributeValue(0); 
			    		 }
			    		 
			    	 }
			    	 
			    	 else if (reader.getLocalName().equals(RDFAlignReader.ENTITY2) ||
			    			 reader.getLocalName().equals(RDFAlignReader.ALIGNMENTENTITY2)){
			    		 
			    		 if (reader.getAttributeCount()>0){
					    	//System.out.println("Att: " + reader.getAttributeValue(0));
				    		iri_str2 = reader.getAttributeValue(0); 	
			    		 }
			    		 
			    	 }
			    	 
			    	 else if (reader.getLocalName().equals(RDFAlignReader.RELATION) ||
			    			 reader.getLocalName().equals(RDFAlignReader.ALIGNMENTRELATION)){
			    		 
			    		 //System.out.println("TExt: " + reader.getElementText());
			    		 relation = reader.getElementText();
			    		 
			    	 }
			    	 
			    	 else if (reader.getLocalName().equals(RDFAlignReader.MEASURE) ||
			    			 reader.getLocalName().equals(RDFAlignReader.ALIGNMENTMEASURE)){
			    		 
			    		 //System.out.println("TExt: " + reader.getElementText());
			    		 confidence = Double.valueOf(reader.getElementText());
			    		 
			    	 }
			    	
		    	}
			    
			    
			}
			else if(reader.getEventType()==XMLStreamConstants.END_ELEMENT){
			
				 if (reader.hasName()){
					 
					 if (reader.getLocalName().equals(RDFAlignReader.CELL)){
						  
						  /*System.out.println(next++);
						  System.out.println(iri_str1);
						  System.out.println(iri_str2);
						  System.out.println(relation);
						  System.out.println(confidence);*/
						  
						  
						  if (relation.equals(">")){
							  dir_relation = MappingObjectStr.SUP;
							  //System.out.println("R2L");
						  }
						  else if (relation.equals("<")){
							  dir_relation = MappingObjectStr.SUB;
							  //System.out.println("L2R");
						  }
						  else if (relation.equals("?")){
							  dir_relation = MappingObjectStr.Flagged;
							  //System.out.println("L2R");
						  }
						  else { //Any other case: ie Hertuda/Hotmatch does not use "="
							  dir_relation = MappingObjectStr.EQ;
							  //System.out.println("=");
						  }
							
						  mappings.add(new MappingObjectStr(iri_str1, iri_str2, confidence, dir_relation));
						  
					  }
				 }
				 
				 //Add to object if everything is ok!
				
			}
			
		    
		    
		    reader.next();
		}//end while

		reader.close();
		
	}

}
