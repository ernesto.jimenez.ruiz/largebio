/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package org.hobbit.largebiobenchmark;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.hobbit.core.components.AbstractDataGenerator;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.core.rabbit.SimpleFileSender;
import org.hobbit.largebiobenchmark.task.Task;
import org.hobbit.largebiobenchmark.util.PlatformConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ernesto
 * Created on 9 Jan 2018
 *
 */
public class DataGenerator extends AbstractDataGenerator {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(DataGenerator.class);
	
	protected String largebio_task;
	
	protected File source_file;
	
	protected File target_file;
	
	protected File reference_file;
	
	private int taskId = 0;
	
	private String task_queue_Name = "largebio_task";
	
	private Task task;
	
	private SimpleFileSender sender;
	
	 @Override
	 public void init() throws Exception {
	      LOGGER.info("Initializing Data Generator '" + getGeneratorId() + "'");
	      super.init();

	      //Get Env variables
	      getEnvVariables();
	      
	      task = new Task(Integer.toString(taskId++));

	    }

	
	
	@Override
	/**
	 * Sets the source and target ontologies, and the reference alignment
	 */
	protected void generateData() throws Exception {
		
		
		LOGGER.info("Generate data.. ");
        sender = null;
        
        try {

        	
        	
			prepareTaskDataset();
			
			
			//Workflow. In HOBBIT the workflow and format of task is rather generic and perhaps more oriented to query answering.
			//Typically a System in HOBBIT gets a source dataset (e.g. source ontology) and a Task that will contain the target dataset 
			//(e.g. target ontology) and expected results (e.g. reference alignment).
			
			
	        //To be set to the Task. Task will also include parameter like matching only classes...
	        prepareReference();
		        
		        
	        //To be sent to the system
	        prepareQueueNames();
	        
	        
	        //To be sent to the TaskGenerator
	        prepareTasks();
		
        } 
        catch (Exception e) {
            LOGGER.error("Exception while sending file to System Adapter or Task Generator(s).", e);
        }
		
	}
	
	
	/**
	 * 
	 * Sets one (or many) Task(s) and sends it to TaskGenerator (source, target and relevant parameters). 
	 * Files itself are sent via the FileSender (default queue name: 'input_files'). Relevant attributes sent as byte[] directly to the TaskGenerator
	 * 
	 * @throws IOException 
	 * 
	 */
	private void prepareTasks() throws IOException {
		
		byte[][] generatedFileArray = new byte[3][];
        // send the file name and its content
		//We send both source and target paths: not necessary for largebio but useful for multfarm or conference
        generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);        
        generatedFileArray[1] = RabbitMQUtils.writeString(source_file.getAbsolutePath());//source
        generatedFileArray[2] = RabbitMQUtils.writeString(target_file.getAbsolutePath()); //target
        
        // convert them to byte[]
        byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
        task.setSourceTarget(generatedFile);
        
        //Set other parameters for the task. i.e. what to match, potentially which URLs, etc.
        //In Largebio we only match classes
        task.setMatchingClassesRequired(true);
        task.setMatchingObjectPropertiesRequired(false);
        task.setMatchingDataPropertiesRequired(false);
        task.setMatchingInstancesRequired(false);
        
        //Task name is the queue name of the task generator to the system
        task.setTaskQueueName(task_queue_Name);

        byte[] data = SerializationUtils.serialize(task);

        
        
        // define a queue name, e.g., read it from the environment
        String queueName = "input_files";
        
        // create the sender
        sender = SimpleFileSender.create(this.outgoingDataQueuefactory, queueName);

        InputStream is_source = null;
        InputStream is_target = null;
        try {
            // create input stream, e.g., by opening a file
            is_source = new FileInputStream(source_file);
            is_target = new FileInputStream(target_file);
            // send data
            sender.streamData(is_source, source_file.getName());
            sender.streamData(is_target, target_file.getName());
            
            
        } catch (Exception e) {
            // handle exception
        } finally {
            IOUtils.closeQuietly(is_source);
            IOUtils.closeQuietly(is_target);
        }

        // close the sender
        IOUtils.closeQuietly(sender);

        sendDataToTaskGenerator(data);
        LOGGER.info("Target data successfully sent to Task Generator.");
		
	}



	/**
	 * 
	 * We prepare the queue names where the files for the benchmark tasks will be sent
	 * This information will be treated in the method "receiveGeneratedData" of the SystemAdapter
	 * Single task benchmark will send only one queue name, while multiple task benchmarks like Conference and Multifarm will send many 
	 *  
	 * @throws IOException 
	 * 
	 */
	private void prepareQueueNames() throws IOException {

		byte[][] generatedFileArray = new byte[2][];
        
		// send format of the files (inherited from hobbit tasks)
        generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
        
        //We send queue names to system to initialize set of threads to deal with the received files from Taskgenerator
        //Send as many queues as tasks (execute din one go) in the benchmark
        generatedFileArray[1] = RabbitMQUtils.writeString(task_queue_Name);
        
        
        // convert them to byte[]
        byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);

        
        // send data to system
        sendDataToSystemAdapter(generatedFile);
        LOGGER.info(source_file.getAbsolutePath() + " (" + (double) source_file.length() / 1000 + " KB) sent to System Adapter.");
		
	}



	/**
	 * @throws IOException 
	 * 
	 */
	private void prepareReference() throws IOException {
		
		 byte[][] generatedFileArray = new byte[3][];
         // send the file name and its content
         generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
         generatedFileArray[1] = RabbitMQUtils.writeString(reference_file.getAbsolutePath());
         generatedFileArray[2] = FileUtils.readFileToByteArray(reference_file);
         // convert them to byte[]
         byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
         
         
         task.setExpectedAnswers(generatedFile);
		
	}



	/**
	 * 
	 */
	private void prepareTaskDataset() {
		
		
		String ontology1;
		String ontology2;
		String reference;
		
		
		
		switch (largebio_task) {
		case PlatformConstants.TASK_FMA_NCI_SMALL:
        case PlatformConstants.TASK_FMA_NCI_SMALL_URI:
        	ontology1="FMA_overlapping_nci.owl";
        	ontology2="NCI_overlapping_fma.owl";
        	reference="reference_fma2nci.rdf";
        	task_queue_Name = "largebio-fma_nci_small_2018";
        	break;
        
        case PlatformConstants.TASK_FMA_NCI_LARGE:
        case PlatformConstants.TASK_FMA_NCI_LARGE_URI:
        	ontology1="FMA.owl";
        	ontology2="NCI.owl";
        	reference="reference_fma2nci.rdf";
        	task_queue_Name = "largebio-fma_nci_whole_2018";
        	break;
        
        case PlatformConstants.TASK_FMA_SNOMED_SMALL:
        case PlatformConstants.TASK_FMA_SNOMED_SMALL_URI:
        	ontology1="FMA_overlapping_snomed.owl";
        	ontology2="SNOMED_overlapping_fma.owl";
        	reference="reference_fma2snomed.rdf";
        	task_queue_Name = "largebio-fma_snomed_small_2018";
        	break;
        
        case PlatformConstants.TASK_FMA_SNOMED_LARGE:
        case PlatformConstants.TASK_FMA_SNOMED_LARGE_URI:
        	ontology1="FMA.owl";
        	ontology2="SNOMED.owl";
        	reference="reference_fma2snomed.rdf";
        	task_queue_Name = "largebio-fma_snomed_whole_2018";
        	break;
        
        case PlatformConstants.TASK_SNOMED_NCI_SMALL:
        case PlatformConstants.TASK_SNOMED_NCI_SMALL_URI:
        	ontology1="SNOMED_overlapping_nci.owl";
        	ontology2="NCI_overlapping_snomed.owl";
        	reference="reference_snomed2nci.rdf";
        	task_queue_Name = "largebio-snomed_nci_small_2018";
        	break;
        	
        case PlatformConstants.TASK_SNOMED_NCI_LARGE:
        case PlatformConstants.TASK_SNOMED_NCI_LARGE_URI:
        	ontology1="SNOMED.owl";
        	ontology2="NCI.owl";
        	reference="reference_snomed2nci.rdf";
        	task_queue_Name = "largebio-snomed_nci_whole_2018";
        	break;
        default:
        	//Small fma-nci
        	ontology1="FMA_overlapping_nci.owl";
        	ontology2="NCI_overlapping_fma.owl";
        	reference="reference_fma2nci.rdf";
        	task_queue_Name = "largebio-fma_nci_small_2018";
        	break;
		}
		
        source_file = new File(PlatformConstants.datasetsPath + File.separator + ontology1);

        target_file = new File(PlatformConstants.datasetsPath + File.separator + ontology2);
        
        reference_file = new File(PlatformConstants.datasetsPath + File.separator + reference);
        
        
	}


	/**
	 * The BenchmarkGenerator tell the task to be executed
	 */
	public void getEnvVariables() {
        LOGGER.info("Getting Data Generator's properites from the environment...");

        Map<String, String> env = System.getenv();
        
        //Given by BenchmarkGenerator and defined in benchmark.ttl file
        largebio_task = (String) getFromEnv(env, PlatformConstants.TASK, "");
        
    }

	
	
    /**
     * A generic method for initialize benchmark parameters from environment
     * variables
     *
     * @param env a map of all available environment variables
     * @param parameter the property that we want to get
     * @param paramType a dummy parameter to recognize property's type
     */
    @SuppressWarnings("unchecked")
    private <T> T getFromEnv(Map<String, String> env, String parameter, T paramType) {
        if (!env.containsKey(parameter)) {
            LOGGER.error(
                    "Environment variable \"" + parameter + "\" is not set. Aborting.");
            throw new IllegalArgumentException(
                    "Environment variable \"" + parameter + "\" is not set. Aborting.");
        }
        try {
            if (paramType instanceof String) {
                return (T) env.get(parameter);
            } else if (paramType instanceof Integer) {
                return (T) (Integer) Integer.parseInt(env.get(parameter));
            } else if (paramType instanceof Long) {
                return (T) (Long) Long.parseLong(env.get(parameter));
            } else if (paramType instanceof Double) {
                return (T) (Double) Double.parseDouble(env.get(parameter));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "Couldn't get \"" + parameter + "\" from the environment. Aborting.", e);
        }
        return paramType;
    }
    
    

    
    
    

}
