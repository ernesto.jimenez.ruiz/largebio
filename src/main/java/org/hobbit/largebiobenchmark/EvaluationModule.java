/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package org.hobbit.largebiobenchmark;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.jena.datatypes.xsd.XSDDatatype;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.hobbit.core.Constants;
import org.hobbit.core.components.AbstractEvaluationModule;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.largebiobenchmark.alignment.HashAlignment;
import org.hobbit.largebiobenchmark.alignment.RDFAlignReader;
import org.hobbit.largebiobenchmark.util.PlatformConstants;
import org.hobbit.vocab.HOBBIT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Based on the SpatialBenchmark eval module, but changed input alignment format
 *
 * @author ernesto
 * Created on 11 Jan 2018
 *
 */
public class EvaluationModule  extends AbstractEvaluationModule {

	private static final Logger LOGGER = LoggerFactory.getLogger(EvaluationModule.class);

    private Property EVALUATION_RECALL = null;
    private Property EVALUATION_PRECISION = null;
    private Property EVALUATION_FMEASURE = null;
    private Property EVALUATION_TIME_PERFORMANCE = null;

    private Model finalModel = ModelFactory.createDefaultModel();

    private int truePositives = 0;
    private int falsePositives = 0;
    private int falseNegatives = 0;

    //private double time_perfomance = 0;
    public long time_performance = 0;
    
    
    @Override
    public void init() throws Exception {
        LOGGER.info("Initializing Evaluation Module started...");
        super.init();

        Map<String, String> env = System.getenv();

        /* time performance */
        if (!env.containsKey(PlatformConstants.EVALUATION_TIME_PERFORMANCE)) {
            throw new IllegalArgumentException("Couldn't get \"" + PlatformConstants.EVALUATION_TIME_PERFORMANCE
                    + "\" from the environment. Aborting.");
        }
        EVALUATION_TIME_PERFORMANCE = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_TIME_PERFORMANCE));

        LOGGER.info("EVALUATION_TIME_PERFORMANCE setted");
        /* recall */
        if (!env.containsKey(PlatformConstants.EVALUATION_RECALL)) {
            throw new IllegalArgumentException("Couldn't get \"" + PlatformConstants.EVALUATION_RECALL
                    + "\" from the environment. Aborting.");
        }
        EVALUATION_RECALL = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_RECALL));

        LOGGER.info("EVALUATION_RECALL setted");

        /* precision */
        if (!env.containsKey(PlatformConstants.EVALUATION_PRECISION)) {
            throw new IllegalArgumentException("Couldn't get \""
                    + PlatformConstants.EVALUATION_PRECISION + "\" from the environment. Aborting.");
        }
        EVALUATION_PRECISION = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_PRECISION));

        LOGGER.info("EVALUATION_PRECISION setted");

        /* fmeasure */
        if (!env.containsKey(PlatformConstants.EVALUATION_FMEASURE)) {
            throw new IllegalArgumentException("Couldn't get \"" + PlatformConstants.EVALUATION_FMEASURE
                    + "\" from the environment. Aborting.");
        }
        EVALUATION_FMEASURE = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_FMEASURE));

        LOGGER.info("EVALUATION_FMEASURE setted");

        LOGGER.info("Initializing Evaluation Module ended...");

    }


	@Override
	protected void evaluateResponse(byte[] expectedData, byte[] receivedData, long taskSentTimestamp,
			long responseReceivedTimestamp) throws Exception {
		
		
		//TODO 
		//ExpectedData and ReceivedData contain the ByteArrays of the OAEI mappings in RDF Alignment format 
		//1. Use ByteArrayInputStream to read OAEI files: see RDFAlignReader
		//2. Get mappings as HashAlignment
		//3. Calculate metrics
    	
    	
    	
        time_performance = responseReceivedTimestamp - taskSentTimestamp;
        if (time_performance < 0) {
            time_performance = 0;
        }
        LOGGER.info("time_performance in ms: " + time_performance);


        LOGGER.info("Read expected data");
        ByteBuffer buffer_exp = ByteBuffer.wrap(expectedData);
        
        //Although not needed we need to read them the buffer in the required order 
        String format = RabbitMQUtils.readString(buffer_exp);
        String path = RabbitMQUtils.readString(buffer_exp);
        byte[] expected = RabbitMQUtils.readByteArray(buffer_exp);
        
        LOGGER.info("Read system data");
        ByteBuffer buffer_sys = ByteBuffer.wrap(receivedData);
        
      //Although not needed we need to read them the buffer in the required order        
        byte[] received = RabbitMQUtils.readByteArray(buffer_sys);
        
        
        
        //Read alignments 
        HashAlignment ref_alignment;
        HashAlignment system_alignment;
                
        //Reference alignments
        if (expected.length > 0) {
        	//read file
        	RDFAlignReader reader = new RDFAlignReader(expected);
        	ref_alignment = reader.getHashAlignment();
        }
        else{
        	ref_alignment = new HashAlignment();
        }        
        LOGGER.info("Largebio reference alignment size: " + ref_alignment.size());
        
        
        
        //System
        LOGGER.info("Read system data");        
        if (received.length > 0) {        	
        	//read file
        	RDFAlignReader reader = new RDFAlignReader(received);
        	system_alignment = reader.getHashAlignment();
        }
        else{
        	system_alignment = new HashAlignment();
        }
        
        
        LOGGER.info("Largebio system alignment size: " + system_alignment.size());
       
       
        
        
        //Calculate metrics
        int[] results = ref_alignment.evaluation(system_alignment);
        
        
        truePositives = results[0];
	    falsePositives = results[1];
	    falseNegatives = results[2];
	    
	    
	    LOGGER.info("COMPARISON-> TP  FP  FN:" + truePositives + " " + falsePositives + " " + falseNegatives);
		
	}
	
	@Override
    public Model summarizeEvaluation() throws Exception {
        LOGGER.info("Summary of Evaluation begins.");

        if (this.experimentUri == null) {
            Map<String, String> env = System.getenv();
            this.experimentUri = env.get(Constants.HOBBIT_EXPERIMENT_URI_KEY);
        }

        double recall = 0.0;
        double precision = 0.0;
        double fmeasure = 0.0;

        if ((double) (this.truePositives + this.falseNegatives) > 0.0) {
            recall = (double) this.truePositives / (double) (this.truePositives + this.falseNegatives);
        }
        if ((double) (this.truePositives + this.falsePositives) > 0.0) {
            precision = (double) this.truePositives / (double) (this.truePositives + this.falsePositives);
        }
        if ((double) (recall + precision) > 0.0) {
            fmeasure = (double) (2.0 * recall * precision) / (double) (recall + precision);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////
        Resource experiment = this.finalModel.createResource(experimentUri);
        this.finalModel.add(experiment, RDF.type, HOBBIT.Experiment);

        Literal timePerformanceLiteral = this.finalModel.createTypedLiteral(this.time_performance, XSDDatatype.XSDlong);
        this.finalModel.add(experiment, this.EVALUATION_TIME_PERFORMANCE, timePerformanceLiteral);

        Literal recallLiteral = this.finalModel.createTypedLiteral(recall,
                XSDDatatype.XSDdouble);
        this.finalModel.add(experiment, this.EVALUATION_RECALL, recallLiteral);

        Literal precisionLiteral = this.finalModel.createTypedLiteral(precision,
                XSDDatatype.XSDdouble);
        this.finalModel.add(experiment, this.EVALUATION_PRECISION, precisionLiteral);

        Literal fmeasureLiteral = this.finalModel.createTypedLiteral(fmeasure,
                XSDDatatype.XSDdouble);
        this.finalModel.add(experiment, this.EVALUATION_FMEASURE, fmeasureLiteral);

        LOGGER.info("Summary of Evaluation is over.");

        return this.finalModel;
    }
	
	
	/**
	 * For testing only
	 * @param max_number
	 */
	private void printMapping(HashAlignment alignment, int max_number, String origin){
		 
        int i=0;
        for (String s : alignment.getSources()){
        	for (String t : alignment.getTargets(s)){
        		LOGGER.info(origin + ": " + s + " " + t);        		
        		if (++i>max_number) return;
        	}
        	 
        }
	}

}
